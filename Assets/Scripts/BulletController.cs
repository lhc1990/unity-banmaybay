﻿using UnityEngine;
using System.Collections;

public class BulletController : MonoBehaviour {

	public float speed = 5;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate(Vector3.up * speed * Time.deltaTime);
		if(transform.position.y > 6.2f){
			Destroy (this.gameObject);
		}
			
	
	}

	void OnCollisionEnter2D(Collision2D col){
		if (col.gameObject.tag == "enemy") {
			Destroy (col.gameObject);
		}
	}
}
