﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {

	float speed = 3;

	// Use this for initialization
	void Start () {
		Quaternion rotation = transform.rotation;
		float z = Random.Range (124, 229);
		rotation = Quaternion.Euler (0, 0, z);
		transform.rotation = rotation;
	}
	
	// Update is called once per frame
	void Update () {

		Vector3 pos = transform.position;
		Vector3 velocity = new Vector3(0,speed * Time.deltaTime,0);
		pos += transform.rotation  * velocity;
		transform.position = pos;
		if (transform.position.y < -6.2f) {
			Destroy (this.gameObject);
		}
	
	}
}
