﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public float speed = 5;
	public GameObject bullet;
	public float maxSpeed = 5f;
	public float rotSpeed = 180f;

	float shipBoundaryRadius = 0.5f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
//		if (Input.GetKey ("a")) {
//			transform.Translate (-Vector3.right * speed * Time.deltaTime);
//		} else if (Input.GetKey("d")){
//			transform.Translate (Vector3.right * speed * Time.deltaTime);
//		} else if (Input.GetKey("s")){
//			transform.Translate (-Vector3.up * speed * Time.deltaTime);
//		} else if (Input.GetKey("w")){
//			transform.Translate (Vector3.up * speed * Time.deltaTime);
//		}
//
		if (Input.GetKey(KeyCode.Space)){
			Instantiate (bullet, transform.position, Quaternion.identity);
		}
		//rotate
		Quaternion rotation = transform.rotation;
		float z = rotation.eulerAngles.z;
		z += Input.GetAxis ("Horizontal") * rotSpeed * Time.deltaTime;
		rotation = Quaternion.Euler (0, 0, z);
		transform.rotation = rotation;

		Vector3 pos = transform.position;
		Vector3 velocity = new Vector3(0,Input.GetAxis("Vertical")* maxSpeed * Time.deltaTime,0);

		pos += rotation * velocity;

		transform.position = pos;
	}
}
