﻿using UnityEngine;
using System.Collections;

public class Spawn : MonoBehaviour {

	public float enemyStart;
	public float enemyDelay;
	public GameObject enemy;

	// Use this for initialization
	void Start () {
		enemyStart = Random.Range (3f, 5f);
		enemyDelay = Random.Range (3f, 5f);
		InvokeRepeating ("spawnEnemy",enemyStart, 1);


	}
	
	// Update is called once per frame
	void Update () {
		//Instantiate (enemy, transform.position, transform.rotation);
	}

	void spawnEnemy(){
		float positionX = Random.Range (-3f, 5f);
		Vector3 initPostion = transform.position;
		initPostion.x = positionX;
		Instantiate (enemy, initPostion, transform.rotation);
	}

}
